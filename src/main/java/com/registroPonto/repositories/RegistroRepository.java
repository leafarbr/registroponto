package com.registroPonto.repositories;

import com.registroPonto.models.Registro;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RegistroRepository extends CrudRepository<Registro, Integer> {
    List<Registro> findAllByUsuarioId(Integer idUsuario);
}
