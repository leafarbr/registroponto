package com.registroPonto.controllers;


import com.registroPonto.models.DTOs.RegistroSaidaDTO;
import com.registroPonto.models.DTOs.UsuarioEntradaDTO;
import com.registroPonto.models.Registro;
import com.registroPonto.models.Usuario;
import com.registroPonto.services.RegistroService;
import com.registroPonto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;


    @PostMapping
    public ResponseEntity<Usuario> Cadastrar(@RequestBody Usuario usuario)
    {
        try {
            Usuario usuarioObjeto = new Usuario();
            usuarioObjeto = usuarioService.cadastrarUsuario(usuario);
            return ResponseEntity.status(201).body(usuario);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Usuario Alterar(@PathVariable int id, @RequestBody UsuarioEntradaDTO usuario)
    {
        try {
            Usuario usuarioObjeto = new Usuario();
            usuarioObjeto = usuarioService.alterarUsuario(id, usuario);
            return usuarioObjeto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Usuario ConsultarUsuarioPorId(@PathVariable int id)
    {
        Usuario usuarioObjeto = usuarioService.consultarUsuarioPorId(id);
        return usuarioObjeto;
    }

    @GetMapping
    public Iterable<Usuario> ConsultarUsuarios()
    {
        Iterable<Usuario> listaUsuarios = usuarioService.ListarUsuarios();
        return listaUsuarios;

    }

}
