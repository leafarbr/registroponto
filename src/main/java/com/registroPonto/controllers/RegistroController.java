package com.registroPonto.controllers;

import com.registroPonto.models.DTOs.RegistroEntradaDTO;
import com.registroPonto.models.DTOs.RegistroSaidaDTO;
import com.registroPonto.models.Registro;
import com.registroPonto.services.RegistroService;
import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/registro")
public class RegistroController {

    @Autowired
    private RegistroService registroService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity registrar(@RequestBody RegistroEntradaDTO registro)
    {
        Registro objRegistro = registroService.registrarPonto(registro);
        return ResponseEntity.status(201).body(registro);
    }

    @GetMapping
    public Iterable<Registro> listarTodosRegistros()
    {
        Iterable<Registro> registro = registroService.consultarTodosRegistros();

        return registro;

    }

    @GetMapping("/{idUsuario}")
    public RegistroSaidaDTO listarRegistros(@PathVariable(name = "idUsuario") Integer idusuario)
    {
        try {
            Iterable<Registro> registro = registroService.consultarRegistrosPorUsuario(idusuario);
            List<Registro> listaregistro = new ArrayList<>();

            listaregistro = (List) registro;

            RegistroSaidaDTO registroSaida = new RegistroSaidaDTO();

            registroSaida.setRegistros(listaregistro);

            registroSaida.setTempoTrabalhado(registroService.calcularTotalTrabalhado(listaregistro));

            return registroSaida;
        }
        catch (RuntimeException exception)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

}
