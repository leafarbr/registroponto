package com.registroPonto.models;

import com.sun.istack.NotNull;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Registro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @NotNull
    private Usuario usuario;

    @NotNull
    private LocalDateTime datahora;

    @NotNull
    private TipoRegistroEnum tipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getDatahora() {
        return datahora;
    }

    public void setDatahora(LocalDateTime datahora) {
        this.datahora = datahora;
    }

    public TipoRegistroEnum getTipo() {
        return tipo;
    }

    public void setTipo(TipoRegistroEnum tipo) {
        this.tipo = tipo;
    }

    public Registro() {
    }


}
