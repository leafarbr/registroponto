package com.registroPonto.models.DTOs;

import com.registroPonto.models.Registro;
import com.registroPonto.models.TipoRegistroEnum;

import java.time.LocalDateTime;
import java.util.List;

public class RegistroSaidaDTO {

    long tempoTrabalhado;

    List<Registro> registros;

    public long getTempoTrabalhado() {
        return tempoTrabalhado;
    }

    public void setTempoTrabalhado(long tempoTrabalhado) {
        this.tempoTrabalhado = tempoTrabalhado;
    }

    public List<Registro> getRegistros() {
        return registros;
    }

    public void setRegistros(List<Registro> registros) {
        this.registros = registros;
    }

    public RegistroSaidaDTO() {
    }
}
