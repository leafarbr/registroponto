package com.registroPonto.models.DTOs;

import com.registroPonto.models.TipoRegistroEnum;

import java.time.LocalDateTime;

public class RegistroEntradaDTO
{
    int id;
    int idusuario;
    LocalDateTime datahora;
    TipoRegistroEnum tipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public LocalDateTime getDatahora() {
        return datahora;
    }

    public void setDatahora(LocalDateTime datahora) {
        this.datahora = datahora;
    }

    public TipoRegistroEnum getTipo() {
        return tipo;
    }

    public void setTipo(TipoRegistroEnum tipo) {
        this.tipo = tipo;
    }

    public RegistroEntradaDTO() {
    }
}
