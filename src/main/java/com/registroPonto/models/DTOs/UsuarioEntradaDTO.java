package com.registroPonto.models.DTOs;

import java.time.LocalDate;

public class UsuarioEntradaDTO
{

    private String nomecompleto;
    private String cpf;
    private String email;

    public String getNomecompleto() {
        return nomecompleto;
    }

    public void setNomecompleto(String nomecompleto) {
        this.nomecompleto = nomecompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UsuarioEntradaDTO() {
    }
}
