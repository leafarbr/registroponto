package com.registroPonto.services;

import com.registroPonto.models.DTOs.RegistroEntradaDTO;
import com.registroPonto.models.Registro;
import com.registroPonto.models.TipoRegistroEnum;
import com.registroPonto.models.Usuario;
import com.registroPonto.repositories.RegistroRepository;
import com.registroPonto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class RegistroService {

    @Autowired
    private RegistroRepository registroRepository;

    @Autowired
    private UsuarioService usuarioService;


    public Registro registrarPonto(RegistroEntradaDTO registro)
    {
        try {
            Usuario objUsuario = usuarioService.consultarUsuarioPorId(registro.getIdusuario());

            Registro objRegistro = new Registro();
            objRegistro.setUsuario(objUsuario);
            objRegistro.setDatahora(registro.getDatahora());
            objRegistro.setTipo(registro.getTipo());

            return registroRepository.save(objRegistro);

        }
        catch (RuntimeException exception)
        {
            throw new RuntimeException(exception.getMessage());
        }
    }

    public List<Registro> consultarRegistrosPorUsuario(Integer idUsuario){
        List<Registro> listaRegistros= registroRepository.findAllByUsuarioId(idUsuario);
        if(!listaRegistros.isEmpty())
            return listaRegistros;
        else
            throw new RuntimeException("Registros não encontrados para esse usuário!");

    }

    public Iterable<Registro> consultarTodosRegistros(){
        Iterable<Registro> listaRegistros = registroRepository.findAll();
        return listaRegistros;
    }

    public long calcularTotalTrabalhado(List<Registro> listaregistros)
    {

        long qtdSegundos = 0;
        Duration duracao;

        if (listaregistros.size() < 2)
        {
            throw new RuntimeException("Só existe um registro, não é possível calcular");
        }

        if (listaregistros.size() % 2 != 0)
        {
            throw new RuntimeException("Quantidade de registros impares, não é possível calcular");
        }

        int qtdlidos = 0;

        while (qtdlidos < listaregistros.size())
        {
            if (listaregistros.get(qtdlidos).getTipo() == TipoRegistroEnum.ENTRADA &&
                listaregistros.get(qtdlidos+1).getTipo() == TipoRegistroEnum.SAIDA) {

                duracao =  Duration.between(listaregistros.get(qtdlidos).getDatahora(),
                                            listaregistros.get(qtdlidos+1).getDatahora());

                qtdSegundos = qtdSegundos + duracao.getSeconds();

                qtdlidos =+2;
            }
            else
            {
                throw new RuntimeException("Registros fora da sequência correta, não é possível calcular");
            }

        }

        return qtdSegundos;
    }

}
