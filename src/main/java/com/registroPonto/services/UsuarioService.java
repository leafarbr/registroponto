package com.registroPonto.services;

import com.registroPonto.models.DTOs.UsuarioEntradaDTO;
import com.registroPonto.models.Usuario;
import com.registroPonto.repositories.UsuarioRepository;
import javassist.bytecode.stackmap.BasicBlock;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import org.aspectj.bridge.IMessage;
import org.aspectj.bridge.IMessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario cadastrarUsuario(Usuario usuario)
    {
        Usuario objetoUsuario = usuarioRepository.save(usuario);
        return objetoUsuario;
    }

    public Usuario alterarUsuario(int id, UsuarioEntradaDTO usuario)
    {
        try {
            Usuario usuarioCadastrado = consultarUsuarioPorId(id);
            usuarioCadastrado.setNomecompleto(usuario.getNomecompleto());
            usuarioCadastrado.setCpf(usuario.getCpf());
            usuarioCadastrado.setEmail(usuario.getEmail());
            Usuario objetoUsuario = usuarioRepository.save(usuarioCadastrado);
            return objetoUsuario;
        }
        catch (RuntimeException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }

    public Usuario consultarUsuarioPorId(int idUsuario)
    {
        Optional<Usuario> optUsuario;
        optUsuario = usuarioRepository.findById(idUsuario);

        if (optUsuario.isPresent())
        {
            return optUsuario.get();
        }
        else
        {
            throw  new RuntimeException("Usuário não localizado no sistema");
        }
    }

    public Iterable<Usuario> ListarUsuarios()
    {
        Iterable<Usuario> listaUsuarios = usuarioRepository.findAll();
        return listaUsuarios;
    }
}
