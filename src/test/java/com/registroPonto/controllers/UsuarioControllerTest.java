package com.registroPonto.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.registroPonto.models.DTOs.RegistroEntradaDTO;
import com.registroPonto.models.DTOs.UsuarioEntradaDTO;
import com.registroPonto.models.Registro;
import com.registroPonto.models.TipoRegistroEnum;
import com.registroPonto.models.Usuario;
import com.registroPonto.services.UsuarioService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    private Usuario usuarioTest;
    private UsuarioEntradaDTO usuarioDTOTest;

    private ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp(){

        usuarioTest = new Usuario();
        usuarioTest.setId(1);
        usuarioTest.setNomecompleto("Pedro Alvares Cabral");
        usuarioTest.setEmail("pedro@gmail.com");
        usuarioTest.setCpf("30020010050");

        usuarioDTOTest = new UsuarioEntradaDTO();
        usuarioDTOTest.setNomecompleto("Pedro Alterado");
        usuarioDTOTest.setEmail("email@email@gmail.com");
        usuarioDTOTest.setCpf("30300033300");
    }

    @Test
    public void testarCadastrarUsuarioComSucesso() throws Exception {

        Mockito.when(usuarioService.cadastrarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
            usuarioTest.setDatacadastro(LocalDate.now());
            return usuarioTest;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuarioTest);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarCadastraUsuarioSemSucesso() throws Exception {
        Mockito.when(usuarioService.cadastrarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto->{
            throw new RuntimeException("Não foi possivel cadastrar o usuário");
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuarioTest);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }





    @Test
    public void testarAlterarUsuarioComSucesso() throws Exception {
        Mockito.when(usuarioService.alterarUsuario(Mockito.anyInt(),Mockito.any(UsuarioEntradaDTO.class))).then(usuarioObjeto->{
            usuarioTest.setNomecompleto("Nome Alterado");
            return usuarioTest;
        });

        ObjectMapper mapper = new ObjectMapper();

        usuarioDTOTest.setNomecompleto("Nome Alterado");
        String jsonDeUsuario = mapper.writeValueAsString(usuarioDTOTest);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomecompleto", CoreMatchers.equalTo("Nome Alterado")));
    }

    @Test
    public void testarAlterarUsuarioNaoEncontrado() throws Exception {
        Mockito.when(usuarioService.alterarUsuario(Mockito.anyInt(),Mockito.any(UsuarioEntradaDTO.class))).then(usuarioObjeto->{
            throw new RuntimeException("Usuário não localizado no sistema");
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuarioDTOTest);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    @Test
    public void testarConsultarUsuarioPorId() throws Exception {
        Mockito.when(usuarioService.consultarUsuarioPorId(Mockito.anyInt())).then(usuarioObjeto -> {
            return usuarioTest;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }


    @Test
    public void testarListarUsuariosExistentes()throws Exception {
        Mockito.when(usuarioService.ListarUsuarios()).then(usuarioObjeto -> {
            List<Usuario> usuarios = new ArrayList<>();
            usuarioTest.setNomecompleto("Teste1");
            usuarios.add(usuarioTest);
            usuarios.add(usuarioTest);
            usuarios.add(usuarioTest);
            return usuarios;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()", CoreMatchers.equalTo(3)));
    }

}