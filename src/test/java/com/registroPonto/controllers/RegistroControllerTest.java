package com.registroPonto.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.registroPonto.models.DTOs.RegistroEntradaDTO;
import com.registroPonto.models.DTOs.UsuarioEntradaDTO;
import com.registroPonto.models.Registro;
import com.registroPonto.models.TipoRegistroEnum;
import com.registroPonto.models.Usuario;
import com.registroPonto.services.RegistroService;
import com.registroPonto.services.UsuarioService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.persistence.EnumType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(RegistroController.class)
public class RegistroControllerTest {

    @MockBean
    private RegistroService registroService;

    @Autowired
    private MockMvc mockMvc;

    private Registro registroTest;
    private Usuario usuarioTest;
    private RegistroEntradaDTO registroEntradaDTO;

    private ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp(){

        registroEntradaDTO = new RegistroEntradaDTO();

        registroEntradaDTO.setId(1);
        registroEntradaDTO.setIdusuario(2);
        registroEntradaDTO.setDatahora(LocalDateTime.now());
        registroEntradaDTO.setTipo(TipoRegistroEnum.ENTRADA);

        usuarioTest = new Usuario();

        usuarioTest.setId(1);
        usuarioTest.setNomecompleto("Pedro Alvares Cabral");
        usuarioTest.setEmail("pedro@gmail.com");
        usuarioTest.setCpf("30020010050");

        registroTest = new Registro();
        registroTest.setId(1);
        registroTest.setUsuario(usuarioTest);
        registroTest.setTipo(TipoRegistroEnum.ENTRADA);
    }

    @Test
    public void testarRegistrarPontoComSucesso() throws Exception {

        Mockito.when(registroService.registrarPonto(Mockito.any(RegistroEntradaDTO.class))).then(registroObjeto -> {
            registroTest.setDatahora(LocalDateTime.now());
            return registroTest;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeRegistro = mapper.writeValueAsString(registroTest);

        mockMvc.perform(MockMvcRequestBuilders.post("/registro")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeRegistro))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarConsultarTodososRegistros()throws Exception {
        Mockito.when(registroService.consultarTodosRegistros()).then(registroObjeto -> {
            List<Registro> registros = new ArrayList<>();

            registroTest.setDatahora(LocalDateTime.now());
            registros.add(registroTest);
            registros.add(registroTest);
            registros.add(registroTest);
            return registros;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/registro"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()", CoreMatchers.equalTo(3)));

    }

    @Test
    public void testarConsultarTodososRegistrosPorId()throws Exception {
        Mockito.when(registroService.consultarRegistrosPorUsuario(Mockito.anyInt())).then(registroObjeto -> {
            List<Registro> registros = new ArrayList<>();

            registroTest.setDatahora(LocalDateTime.now());
            registros.add(registroTest);
            registros.add(registroTest);
            return registros;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/registro/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()", CoreMatchers.equalTo(2)));
    }

    @Test
    public void testarConsultarTodososRegistrosPorIdNãoLocalizado()throws Exception {
        Mockito.when(registroService.consultarRegistrosPorUsuario(Mockito.anyInt())).then(usuarioObjeto->{
            throw new RuntimeException("Usuário não localizado no sistema");
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(registroTest);

        mockMvc.perform(MockMvcRequestBuilders.get("/registro/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
