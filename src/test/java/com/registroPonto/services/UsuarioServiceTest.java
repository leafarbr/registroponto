package com.registroPonto.services;

import com.registroPonto.models.DTOs.UsuarioEntradaDTO;
import com.registroPonto.models.Usuario;
import com.registroPonto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    private Usuario usuarioTest;
    private UsuarioEntradaDTO usuarioDTOTest;

    @BeforeEach
    public void setUp()
    {
        usuarioTest = new Usuario();
        usuarioTest.setId(1);
        usuarioTest.setNomecompleto("Pedro Alvares Cabral");
        usuarioTest.setEmail("pedro@gmail.com");
        usuarioTest.setCpf("30020010050");
        usuarioTest.setDatacadastro(LocalDate.now());

        usuarioDTOTest = new UsuarioEntradaDTO();
        usuarioDTOTest.setNomecompleto("Pedro Alvares Cabral");
        usuarioDTOTest.setEmail("pedro@gmail.com");
        usuarioDTOTest.setCpf("30020010050");

    }

    @Test
    public void TestarCadastrarUsuario()
    {
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuarioTest);

        Usuario novoUsuario = usuarioService.cadastrarUsuario(usuarioTest);

        Assertions.assertEquals(novoUsuario.getCpf(), usuarioTest.getCpf());

    }

    @Test
    public void testarAlterarUsuarioComSucesso() {

        Mockito.when(usuarioRepository.findById((Mockito.anyInt()))).thenReturn(Optional.of(usuarioTest));
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuarioTest);

        Usuario objetoUsuario = usuarioService.alterarUsuario(usuarioTest.getId(),usuarioDTOTest);

        Assertions.assertEquals(objetoUsuario, usuarioTest);

    }

    @Test
    public void testarAlterarUsuarioNãoLocalizado(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.alterarUsuario(1,usuarioDTOTest);
                });
    }

    @Test
    public void testarBuscarUsuarioPorIdEncontrado()
    {

        Optional<Usuario> optionalUsuario = Optional.of(usuarioTest);

        Mockito.when(usuarioRepository.findById(1)).thenReturn(optionalUsuario);

        Usuario usuarioCadastrado = usuarioService.consultarUsuarioPorId(1);

        Assertions.assertEquals(usuarioCadastrado,optionalUsuario.get());

    }

    @Test
    public void testarBuscarUsuarioPorIdNãoEncontrado()
    {

        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.consultarUsuarioPorId(1);
                });
    }

    @Test
    public void testarListarUsuarios()
    {
        Iterable<Usuario> usuarios = Arrays.asList(usuarioTest);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuariosCadastrados = usuarioService.ListarUsuarios();

        Assertions.assertEquals(usuariosCadastrados, usuarios);
    }
}
