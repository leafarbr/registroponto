package com.registroPonto.services;

import com.registroPonto.models.DTOs.RegistroEntradaDTO;
import com.registroPonto.models.DTOs.UsuarioEntradaDTO;
import com.registroPonto.models.Registro;
import com.registroPonto.models.TipoRegistroEnum;
import com.registroPonto.models.Usuario;
import com.registroPonto.repositories.RegistroRepository;
import com.registroPonto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@SpringBootTest
public class RegistroServiceTest {

    @MockBean
    private RegistroRepository registroRepository;

    @Autowired
    private RegistroService registroService;

    private Registro registroTest;
    private Usuario usuarioTest;
    private RegistroEntradaDTO registroEntradaDTO;


    @BeforeEach
    public void setUp() {

        usuarioTest = new Usuario();
        usuarioTest.setId(1);
        usuarioTest.setNomecompleto("Pedro Alvares Cabral");
        usuarioTest.setEmail("pedro@gmail.com");
        usuarioTest.setCpf("30020010050");
        usuarioTest.setDatacadastro(LocalDate.now());

        registroTest = new Registro();
        registroTest.setId(1);
        registroTest.setUsuario(usuarioTest);
        registroTest.setDatahora(LocalDateTime.now());
        registroTest.setTipo(TipoRegistroEnum.ENTRADA);

        registroEntradaDTO = new RegistroEntradaDTO();
        registroEntradaDTO.setId(1);
        registroEntradaDTO.setIdusuario(1);
        registroEntradaDTO.setDatahora(LocalDateTime.now());
        registroEntradaDTO.setTipo(TipoRegistroEnum.ENTRADA);

    }

    @Test
    public void testarRegistrarPonto()
    {
        Mockito.when(registroRepository.save(Mockito.any(Registro.class))).thenReturn(registroTest);

        Registro novoRegistro = registroService.registrarPonto(registroEntradaDTO);

        Assertions.assertEquals(novoRegistro.getDatahora(), registroTest.getDatahora());

    }


    @Test
    public void testarBuscarRegistrorPorIdUsuario()
    {

        List<Registro> listaRegistros = new ArrayList<>();
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);
        Mockito.when(registroRepository.findAllByUsuarioId(Mockito.anyInt())).thenReturn(listaRegistros);

        List<Registro> listaRegistrosObjeto = registroService.consultarRegistrosPorUsuario(Mockito.anyInt());

        Assertions.assertEquals(listaRegistrosObjeto,listaRegistros );


    }

    @Test
    public void testarBuscarRegistrorPorIdUsuarioNãoEncontrado()
    {

        Mockito.when(registroRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    registroService.consultarRegistrosPorUsuario(1);
                });
    }

    @Test
    public void testarListarRegistros()
    {
        Iterable<Registro> registros = Arrays.asList(registroTest);
        Mockito.when(registroRepository.findAll()).thenReturn(registros);

        Iterable<Registro> registrosCadastrados = registroService.consultarTodosRegistros();

        Assertions.assertEquals(registrosCadastrados, registros);
    }

    @Test
    public void testarCalcularTotalTrabalhadoRegistrosInsuficiente() throws Exception
    {
        List<Registro> listaRegistros = new ArrayList<>();
        listaRegistros.add(registroTest);

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    long segundosTrabalhados = registroService.calcularTotalTrabalhado(listaRegistros);
                });

    }

    @Test
    public void testarCalcularTotalTrabalhadoRegistrosImpares() throws Exception
    {
        List<Registro> listaRegistros = new ArrayList<>();
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    long segundosTrabalhados = registroService.calcularTotalTrabalhado(listaRegistros);
                });

    }

    @Test
    public void testarCalcularTotalTrabalhadoSequenciaErrada() throws Exception
    {
        List<Registro> listaRegistros = new ArrayList<>();
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);
        listaRegistros.add(registroTest);

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    long segundosTrabalhados = registroService.calcularTotalTrabalhado(listaRegistros);
                });

    }

    @Test
    public void testarCalcularTotalTrabalhadoSucesso() throws Exception
    {
        long qtdSegundos;

        List<Registro> listaRegistros = new ArrayList<>();

        Registro registroEntrada = new Registro();
        Registro registroSaida = new Registro();

        registroEntrada.setDatahora(LocalDateTime.now());
        registroEntrada.setTipo(TipoRegistroEnum.ENTRADA);

        registroSaida.setDatahora(LocalDateTime.now().plusSeconds(9000));
        registroSaida.setTipo(TipoRegistroEnum.SAIDA);

        listaRegistros.add(registroEntrada);
        listaRegistros.add(registroSaida);

        qtdSegundos = registroService.calcularTotalTrabalhado(listaRegistros);

        Assertions.assertEquals(qtdSegundos, 9000);

    }

}
