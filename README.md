## Usuario

### GET /usuario
Exibe a lista de usuários cadastrados no sistema.

**Response 200**
```json
[
    {
        "id": 1,
        "nomecompleto": "Maria Nome Completo",
        "cpf": "30030030009",
        "email": "usuario@gmail.com",
        "datacadastro": "2020-07-04"
    },
    {
        "id": 2,
        "nomecompleto": "Joao Nome Completo",
        "cpf": "30030030009",
        "email": "usuario@gmail.com",
        "datacadastro": "2020-07-04"
    },
    {
        "id": 3,
        "nomecompleto": "Rafael Final",
        "cpf": "30030030009",
        "email": "usuario@gmail.com",
        "datacadastro": "2020-07-04"
    }
]
```
### POST /usuario
Cadastra um novo usuario no sistema.

**Request Body**
```json
{

    "nomecompleto" : "Rafael Final",
    "cpf" : "30030030009",
    "email" : "usuario@gmail.com",
    "datacadastro" : "2020-07-04"
}
```

**Response 201**
```json
{
    "id": 3,
    "nomecompleto": "Rafael Final",
    "cpf": "30030030009",
    "email": "usuario@gmail.com",
    "datacadastro": "2020-07-04"
}
```

### PUT /usuario/{idusuario}
Atualiza os dados de um usuario. 

**Request Body**
```json
{
    "nomecompleto" : "Rafael Final Alterado",
    "cpf" : "30030030009",
    "email" : "usuario@gmail.com"
}
```
**Response 200**
```json
{
    "id": 3,
    "nomecompleto": "Rafael Final Alterado",
    "cpf": "30030030009",
    "email": "usuario@gmail.com",
    "datacadastro": "2020-07-04"
}
```

## Registro

### POST /registro
Cadastra um novo registro de ponto do usuário.

**Request Body**
```json
{
    "idusuario" : 3,
    "datahora" : "2020-07-04T13:40:00",
    "tipo" : 1
}
```

**Response 201**
```json
{
    "id": 24,
    "usuario": {
        "id": 3,
        "nomecompleto": "Rafael Final Alterado",
        "cpf": "30030030009",
        "email": "usuario@gmail.com",
        "datacadastro": "2020-07-04"
    },
    "datahora": "2020-07-04T13:40:00",
    "tipo": "SAIDA"
}
```
### GET /registro/{idUsuario}
Exibe todos os registros de um usuário do sistema e o total trabalhado em segundos

**Response 200**
```json
{
    "tempoTrabalhado": 360,
    "registros": [
        {
            "id": 23,
            "usuario": {
                "id": 3,
                "nomecompleto": "Rafael Final",
                "cpf": "30030030009",
                "email": "usuario@gmail.com",
                "datacadastro": "2020-07-04"
            },
            "datahora": "2020-07-04T13:34:00",
            "tipo": "ENTRADA"
        },
        {
            "id": 24,
            "usuario": {
                "id": 3,
                "nomecompleto": "Rafael Final",
                "cpf": "30030030009",
                "email": "usuario@gmail.com",
                "datacadastro": "2020-07-04"
            },
            "datahora": "2020-07-04T13:40:00",
            "tipo": "SAIDA"
        }
    ]
}
```



